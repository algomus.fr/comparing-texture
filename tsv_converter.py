"""
This module aims at converting symbolically encoded files into
tab-separated-values (TSV) format, with each line representing
a note, as a 'note dictionary'.

Target corpora are:
- Annotated Mozart Sonatas [Hentschel et al. 2021]
- TAVERN dataset [Devaney et al. 2015]

License
-------
This file is part of "Comparing Texture" http://algomus.fr/code 
Copyright (C) 2021-2023 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Comparing Texture" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Comparing Texture" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Comparing Texture". If not, see
http://www.gnu.org/licenses/
"""
import csv
from fractions import Fraction
from itertools import chain
import os

import music21 as m21



####################
# Global variables #
####################

#TODO: faire le tri dans ces variables globales et commentaires
OUTPUT_DIR = "." #os.path.join("..", "data", "texturaltsv")

#DURATIONTYPE_TO_GRACENOTE = {  # For conversion to DCML format
#    "half": "grace2", "quarter": "grace4", "eighth": "grace8",
#    "16th": "grace16", "32nd": "grace32", "64th": "grace64", "128th": "grace128"
#}
#TIETYPE_TO_INT = {  # For conversion to DCML format
#    'start': 1, 'continue': 0, 'stop': -1 #(0? TO CHECK)
#}

####
# Read TSV files
####

def extract_notes(filepath, attributes=None, *measure_counters):
    """
    Return all the notes dictionaries in given
    measures from given piece.

    Parameters
    ----------
    filepath : str
        Path to target notelist TSV file to be parsed.
    attributes : list of str
        By default, retrieve all the attributes.
        List of names of attributes to retrieve for each note.
    measure_counters: int
        Measure counters ('mc' as defined in https://github.com/DCMLab/mozart_piano_sonatas/tree/main/notes)
        in which the notes need to be extracted. If none is given,
        retrieve all the notes.

    Returns
    -------
    list of dict
        The list of all the notes in targeted measures
    """
    # Check file path
    if not os.path.exists(filepath):
        #return ValueError("File not found.")
        print("File not found:", filepath)
        return []

    with open(filepath) as tsv_file:
        reader = csv.DictReader(tsv_file, delimiter='\t')
        if measure_counters:
            notes = [note for note in reader if int(note['mc']) in measure_counters]
        else:
            notes = [note for note in reader]
        
    if attributes:
        for i, notedict in enumerate(notes):
            # Build new note dictionary with target columns only
            new_notedict = {}
            for attribute_name in attributes:
                new_notedict[attribute_name] = notedict[attribute_name]
            # Replace original note dictionary by filtered version
            notes[i] = new_notedict

    return notes

def extract_labels(filepath: str):
    """
    Extract labels from given Tab-separated-value file.
    
    Parameters
    ----------
    filepath: str
        Path to target TSV file with labels in a column 'label'.

    Returns
    -------
    texture_str_list: list of str
        List of annotated textural labels.
    """
    # Check file path
    if not os.path.exists(filepath):
        return ValueError("File not found.")

    texture_str_list = []
    with open(filepath) as tsv_file:
        measures = csv.DictReader(tsv_file, delimiter="\t")
        for measure in measures:
            texture_str_list.append(measure['label'])

    return texture_str_list


####
# Write TSV-note file
#############

def piece_to_note_tsv(
        piece,
        output_name=None,
        output_dir=OUTPUT_DIR,
        skip_cadenza=False
        ):
    """
    Generate and save a TSV file with notes information.

    Parameters
    ----------
    piece: str or Path object
        Path to encoded score to be parsed.
        Accepted formats include .kern and .musicxml.
    output_name: str, optional
        Name of the output file. By default, name the output after the
        basename of 'piece' argument. If not specified, '.tsv' extension
        is automatically added.
    output_dir: str or os.path.Path object
        Path to the directory in which the output is saved.
    skip_cadenza: bool, optional
        If True, ignore unmetered measures like cadenza. Defaults to False.

    Returns
    -------
    None

    Notes
    -----
    Adapted from this format: https://github.com/DCMLab/mozart_piano_sonatas/blob/main/notes/K279-1.tsv
    """
    # Extract data
    stream = m21.converter.parse(piece)

    # Initialization
    columns = [
        "mc",
        "mn", "timesig", "barduration",
        "onset", "staff", #"voice",
        "duration", "gracenote", #"nominal_duration", #"scalar",
        "tied", "tpc", "midi", "step" #"volta"
    ]
    standard_time_signature_list = [ # Used to exclude cadenza
        '2/2',
        '2/4', '3/4', '4/4',
        '3/8', '6/8', '9/8', '12/8',
    ]

    def add_note_info(note, base_dict, onset=None):
        """
        Return a new dictionary containing information 
        extracted from given note (note.Note object) 
        and information already contained in base_dict (dictionary)

        Parameters
        ----------
        note: music21.note.Note object
            Note from which information are extracted
        base_dict: dict
            Dictionary with precomputed information on the context
            of given note (in general: measure counter, measure number,
            time signature, staff)
        onset: float
            Add onset value, i.e. position in the measure.
            Defaults to None. Used when onset value needs to be extracted
            from Chord object. If no value is given, this information is
            directly extracted from given note argument.
        
        Return
        ------
        dict
            updated dictionary of notes information.
        """
        note_info = base_dict.copy()

        # Position and duration information
        note_info["onset"] = onset or note.offset
        note_info["duration"] = note.duration.quarterLength
        # Alternate version for DCML format:
        #Fraction(measure.duration.quarterLength).limit_denominator(2048) / 4


        # Additional information for DCML format
        #note_info["nominal_duration"] = Fraction(
        #    note.duration.type #TODO: handle type automatic conversion ?
        #).limit_denominator(2048)
        #note_info["scalar"] = note_info["duration"] / note_info["nominal_duration"]

        note_info["gracenote"] = note.duration.type if note.duration.isGrace else None
        # Alternate version with conversion to DCML format
        #if not note.duration.isGrace:
        #    # Not a grace note
        #    note_info["gracenote"] = None
        #elif note.duration.type in DURATIONTYPE_TO_GRACENOTE:
        #    # Valid grace note
        #    note_info["gracenote"] = DURATIONTYPE_TO_GRACENOTE[note.duration.type]
        #else:
        #    # Grace note not handled
        #    print("Unknown grace note duration type:", note.duration.type)
        #    note_info["gracenote"] = "grace?"

        if note.tie:
            note_info["tied"] = note.tie.type
        else:
            note_info["tied"] = None
        # Alternate version with conversion to DCML format
        #if not note.tie:
        #    # Not a tied note
        #    note_info["tied"] = None
        #elif note.tie.type in TIETYPE_TO_INT:
        #    # Valid tied note
        #    note_info["tied"] = TIETYPE_TO_INT[note.tie.type]
        #else:
        #    # Tied note not handled
        #    print("Unknown tie type:", note.tie.type)
        #    note_info["tied"] = "?"

        # Pitch information
        #note_info["tpc"] = ... #TODO: tonal pitch class
        note_info["midi"] = note.pitch.midi  # MIDI : 60:C4, 61:C#4 or Db4, 62:D4...
        note_info["step"] = note.pitch.step  # Without alteration: C for C, Cb, C##...

        return note_info


    note_list = []
    current_time_signature = None
    # Parse entire score
    for i_part, part in enumerate(stream.getElementsByClass(m21.stream.Part)):
        #print(part) #debug
        for i_measure, measure in enumerate(part.getElementsByClass(m21.stream.Measure)):

            #print("  Measure :", i_measure) #debug
            #measure.plot() #debug
            #input("Press enter to continue") #debug

            # Information about the measure, the context
            measure_info = {} # To store information

            # Context information
            measure_info["mn"] = measure.number
            measure_info["mc"] = i_measure+1
            # Time signature and key signature
            if measure.timeSignature:
                # Log time signature: following measure will have the same, if not specified otherwise.
                current_time_signature = measure.timeSignature.ratioString # For example "4/4"
            if current_time_signature is None:
                print(f"INFO: Skipped measure {i_measure+1} (mc) in {piece} without time signature.")
                continue

            if skip_cadenza and current_time_signature not in standard_time_signature_list:
                # Skip measures with unusual meters
                print(f"INFO: Skipped cadenza: measure {i_measure+1} (mc) in {piece} with meter {current_time_signature}")
                continue
            measure_info["timesig"] = current_time_signature
            measure_info["barduration"] = measure.duration.quarterLength # Actual duration of the bar
            measure_info["staff"] = i_part+1
            #measure_info["voice"] = # IN MEASURE/GLOBAL STREAM
            #measure_info["volta"] = # IN MEASURE


            # Get notes from the measure
            note_iter = measure.getElementsByClass(m21.note.GeneralNote)
            # Add notes from internal voices
            for voice in measure.getElementsByClass(m21.stream.Voice):
                note_iter = chain(note_iter, voice.getElementsByClass(m21.note.GeneralNote))

            # Loop on notes
            for element in note_iter:
                #print("   ", element) #debug
                if not (element.isNote or element.isChord):
                    # Unhandled element: Rest, NoChord...
                    #print("Unhandled element:", element) #debug
                    continue

                # Information about the very note(s)
                if element.isNote:
                    # Directly extract needed information from the note
                    note_list.append(add_note_info(
                        note=element,
                        base_dict=measure_info
                    ))
                elif element.isChord:
                    # Need to extract individual notes
                    for note in element:
                        if note.isNote:
                            note_list.append(add_note_info(
                                note,
                                onset=element.offset, # Take onset from the Chord
                                base_dict=measure_info
                            ))
                        else:
                            print("Unknown element in Chord :", note)
    # Sort final list of notes
    # Priority: ordered by measure, by strarting_time in the measure (onset),
    # with grace notes first, and from low to high register (midi pitch)
    note_list.sort(key= lambda x:x["midi"])
    note_list.sort(key= lambda x:(x["gracenote"] or ""), reverse=True)
    note_list.sort(key= lambda x:x["onset"])
    note_list.sort(key= lambda x:x["mc"])

    # Create output path
    if output_name is None: 
        piece_name = os.path.splitext( # Remove extension
            os.path.basename( # Remove full path / folder
                piece
            )
        )[0]
        output_name = str(piece_name) + "_notes"
    if output_name[-4:] != ".tsv":  # Add tsv extension
        output_name += ".tsv"
    output_path = os.path.join(output_dir, output_name)

    # Save file
    with open(output_path, 'w', newline='\n') as tsv_file:
        writer = csv.DictWriter(
            tsv_file,
            fieldnames= columns,
            delimiter='\t' # TAB separator
        )
        writer.writeheader()
        for note in note_list:
            writer.writerow(note)



########
# Main #
########

if __name__ == "__main__":
    # Test from TAVERN dataset in .kern format
    #test_file = os.path.join("..", "TAVERN", "Mozart", "K455", "Krn", "K455.krn")
    #generate_note_tsv(test_file)
    pass
