"""
This module contains functions to extract textural descriptors from
note-dict TSV files.

This file is adapted from "Symbolic Texture Dataset"
http://algomus.fr/code 

License
-------
This file is part of "Comparing Texture" http://algomus.fr/code 
Copyright (C) 2021-2023 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Comparing Texture" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Comparing Texture" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Comparing Texture". If not, see
http://www.gnu.org/licenses/
"""
import csv
import os

import numpy as np
from descriptors import Pitches, Onsets, Slices
from dist_huron import onset_synchrony, semblant_motions



####
# Generate descriptor files
####

def extract_descriptors(
        note_list,
        output_file="test.tsv",
        whole_fractions_convention=True,
        verbose=True):
    """
    Parameters
    ----------
    note_list : list of dict
        List of note dictionaries as extracted in TSV representation #TODO: details
    output_file : str
        path to the csv file in which the information
        will be written.
    whole_fractions_convention : bool
        If True, parsed onsets and durations are encoded as fractions
        of whole notes (1/4 for quarter notes), as in Annotated Mozart
        Sonatas. Else, onsets and durations are in quarter lentgh (1.0
        for quarter notes). Defaults to True.
    """
    meta_information = [
        #"m-measure_number", # replaced by measure_counter, here
        "m-measure_counter",
        "m-length_beat",
        "m-beat_duration"
    ]
    descriptor_names = [
        # Vertical density
        "n_voices_per_beat_avg",
        "n_voices_per_onset_std",
        # Horizontal density
        "n_onsets",
        "regularity_std",
        # Huron-based features
        "onset_synchrony",
        "semblant_motions",
        # Chroma values
        "chroma0", "chroma1", "chroma2", "chroma3",
        "chroma4", "chroma5", "chroma6", "chroma7",
        "chroma8", "chroma9", "chroma10", "chroma11"
    ]

    # Loop on data
    all_measures = list(set([int(note["mc"]) for note in note_list]))
    all_measures.sort()

    descriptors_list = []
    for i_measure in all_measures:
        descriptors = {}

        if verbose:
            print("Measure", i_measure, end='\r')
        # Select only current measure
        notes = [
            note for note in note_list
            if int(note['mc']) == i_measure
        ]
        if notes and "barduration" in notes[0]:
            # Get the real duration of the bar (e.g. for anacrusis) if existing.
            if '/' in notes[0]["barduration"]:
                # Fraction
                bar_duration = (
                    float(notes[0]["barduration"].split('/')[0])
                    / float(notes[0]["barduration"].split('/')[1])
                )
            else:
                bar_duration = float(notes[0]["barduration"])
        else:
            bar_duration = None # not known

        # Build intermediary structures
        pitches = Pitches(notes, length=bar_duration)
        beat_duration = pitches.beat_duration
        length_quarter = pitches.length_quarter
        length_beat = pitches.length_beat
        onsets = Onsets(
            notes,
            length=length_quarter,
            beat_duration=beat_duration,
            whole_fractions_convention=whole_fractions_convention
        )
        slices = Slices(
            notes,
            length=length_quarter,
            beat_duration=beat_duration,
            whole_fractions_convention=whole_fractions_convention
        )

        # Add meta information
        descriptors['m-measure_counter'] = i_measure
        descriptors['m-length_beat'] = length_beat
        descriptors['m-beat_duration'] = beat_duration

        # Add descriptors
        # Vertical density
        n_voices_per_beat_avg, _, _, _, _ = slices.stats_n_voices()
        descriptors['n_voices_per_beat_avg'] = n_voices_per_beat_avg
        _, n_voices_per_onset_std, _, _, _ = onsets.stats_n_voices()
        descriptors['n_voices_per_onset_std'] = n_voices_per_onset_std
        # Horizontal density
        descriptors['n_onsets'] = onsets.n_onsets()
        _, regularity_std, _, _, _ = onsets.stats_regularity()
        descriptors['regularity_std'] = regularity_std

        # Huron-based features
        descriptors['semblant_motions'] = semblant_motions(slices.slice_dict)
        descriptors['onset_synchrony'] = onset_synchrony(
            onsets.onset_dict, slices.slice_dict,
            length=length_quarter
        )

        # Chroma descriptors
        chroma_values = pitches.chroma()
        for i, chroma_value in enumerate(chroma_values):
            descriptors[f'chroma{i}'] = chroma_value

        # Save descriptors for this measure
        descriptors_list.append(descriptors) 

    # Save results in TSV file

    with open(output_file, 'w', newline='\n') as tsv_file:
        writer = csv.DictWriter(
            tsv_file,
            fieldnames=meta_information+descriptor_names,
            delimiter='\t'
        )
        writer.writeheader()
        for descriptors in descriptors_list:
            writer.writerow(descriptors)


####
# Read descriptors
####

def extract_descriptor_list(filename, measure_number, descriptor_names=None):
    """
    Return an vector of pre-computed symbolic music features in target measure.

    Parameters
    ----------
    filename : str
        Path to target TSV descriptor file to be parsed.
    measure_number : int
        Target measure number.
    descriptor_names : list of str
        Name of the descriptors to be retrieved from target file.
        If none is given, retrieve all the descriptors.

    Returns
    -------
    np.array
        The descriptors in target measure.
    """
    # Check file path
    if not os.path.exists(filename):
        return ValueError(f"File not found: {filename}")

    with open(filename) as tsv_file:
        reader = csv.DictReader(tsv_file, delimiter='\t')
        descriptor_dict = {}
        # Retrieve target line
        for measure in reader:
            if int(measure['m-measure_number']) == measure_number:
                descriptor_dict = measure
                break

    if not descriptor_dict:
        raise RuntimeError(
            f"Measure number {measure_number} not found in {filename}"
        )

    if not descriptor_names: # By default, take all symbolic descriptors
        descriptor_list = [
            value
            for key, value in descriptor_dict.items()
            if not key.startwith("m-")
        ]
    else: # Otherwise, only select target names
        descriptor_list = [
            descriptor_dict[descriptor_name]
            for descriptor_name in descriptor_names
        ]

    return descriptor_list

def extract_descriptor_array(filename, descriptor_names, weights=None):
    """
    Return an array of pre-computed symbolic music descriptors in all the 
    measures of the piece.

    Parameters
    ----------
    filename : str
        Path to target TSV descriptor file to be parsed.
    descriptor_names : list of str
        Name of the descriptors to be retrieved from target file.
    weights: list of float, optional
        Weight for each of the descriptors in descriptor_names.
        By default, all descriptors have the same weight.

    Returns
    -------
    np.array
        The array of descriptor, of size N*D, where N is the number of measures
        in target piece and D is the number of descriptors.
    """
    # Check file path
    if not os.path.exists(filename):
        raise ValueError(f"File not found: {filename}")

    with open(filename) as tsv_file:
        reader = csv.DictReader(tsv_file, delimiter='\t')
        reader_list = list(reader)
        descriptor_array = np.zeros((len(reader_list), len(descriptor_names)))
        for i, line in enumerate(reader_list):
            for j, descriptor_name in enumerate(descriptor_names):
                descriptor_array[i,j] = line[descriptor_name]

    if weights:
        for j, weight in enumerate(weights):
            descriptor_array[:,j] *= weight

    return descriptor_array
