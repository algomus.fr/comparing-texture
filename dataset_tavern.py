"""
This module contains file architecture of the TAVERN dataset and tools
to manipulate and convert it.


License
-------
This file is part of "Comparing Texture" http://algomus.fr/code 
Copyright (C) 2021-2023 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Comparing Texture" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Comparing Texture" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Comparing Texture". If not, see
http://www.gnu.org/licenses/
"""
import os
import re

import music21 as m21

from tsv_converter import extract_notes, piece_to_note_tsv
from extract_descriptors import extract_descriptors


####
# Global variables
####

TAVERN_DIR = os.path.join("..", "TAVERN") # Default: find dataset in parent directory
TAVERN_NOTES_DIR = os.path.join(".", "score_tsv")

TAVERN_DESC_DIR = os.path.join(".", "descriptors_tsv", "desc_tavern")

TAVERN_ARCHITECTURE = {
    # <Composer folder>: <Piece folder>
    "Mozart": {
        # <Piece folder>: (<Krn folder>:) <Variation template>
        "K025": {"Krn": { # K25: Nassau
            # <Variation template>: [<Phrase id>]
            "M025_00_PP_score.krn": ["01", "02", "03"],
            "M025_01_PP_score.krn": ["01", "02", "03"],
            "M025_02_PP_score.krn": ["01", "02", "03"],
            "M025_03_PP_score.krn": ["01", "02", "03"],
            "M025_04_PP_score.krn": ["01", "02", "03"],
            "M025_05_PP_score.krn": ["01", "02", "03"],
            "M025_06_PP_score.krn": ["01", "02", "03"],
            "M025_07_PP_score.krn": ["01", "02", "03"]
        }},
        "K179": {"Krn": {
            "M179_00_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M179_01_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M179_02_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M179_03_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M179_04_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M179_05_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M179_06_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M179_07_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M179_08_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M179_09_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M179_10_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M179_11_PP_score.krn": ["01", "02", "03", "04", "05",
                                      "06", "07", "08", "09", "10"],
            "M179_12_PP_score.krn": ["01", "02", "03", "04", "05"]
        }},
        "K265": {"Krn": { # Ah! vous dirai-je Maman
            "M265_00_PP_score.krn": ["01", "02", "03"],
            "M265_01_PP_score.krn": ["01", "02", "03"],
            "M265_02_PP_score.krn": ["01", "02", "03"],
            "M265_03_PP_score.krn": ["01", "02", "03"],
            "M265_04_PP_score.krn": ["01", "02", "03"],
            "M265_05_PP_score.krn": ["01", "02", "03"],
            "M265_06_PP_score.krn": ["01", "02", "03"],
            "M265_07_PP_score.krn": ["01", "02", "03"],
            "M265_08_PP_score.krn": ["01", "02", "03"],
            "M265_09_PP_score.krn": ["01", "02", "03"],
            "M265_10_PP_score.krn": ["01", "02", "03"],
            "M265_11_PP_score.krn": ["01", "02", "03"],
            "M265_12_PP_score.krn": ["01", "02", "03", "04"]
        }},
        "K353": {"Krn": {
            "M353_00_PP_score.krn": ["01", "02", "03"],
            "M353_01_PP_score.krn": ["01", "02", "03"],
            "M353_02_PP_score.krn": ["01", "02", "03"],
            "M353_03_PP_score.krn": ["01", "02", "03"],
            "M353_04_PP_score.krn": ["01", "02", "03"],
            "M353_05_PP_score.krn": ["01", "02", "03"],
            "M353_06_PP_score.krn": ["01", "02", "03"],
            "M353_07_PP_score.krn": ["01", "02", "03"],
            "M353_08_PP_score.krn": ["01", "02", "03"],
            "M353_09_PP_score.krn": ["01", "02", "03"],
            "M353_10_PP_score.krn": ["01", "02", "03"],
            "M353_11_PP_score.krn": ["01", "02", "03"],
            "M353_12_PP_score.krn": ["01", "02", "03", "04", "05"]
        }},
        "K354": {"Krn": {
            "M354_00_PP_score.krn": ["01", "02", "03", "04"],
            "M354_01_PP_score.krn": ["01", "02", "03", "04"],
            "M354_02_PP_score.krn": ["01", "02", "03", "04"],
            "M354_03_PP_score.krn": ["01", "02", "03", "04"],
            "M354_04_PP_score.krn": ["01", "02", "03", "04"],
            "M354_05_PP_score.krn": ["01", "02", "03", "04"],
            "M354_06_PP_score.krn": ["01", "02", "03", "04"],
            "M354_07_PP_score.krn": ["01", "02", "03", "04"],
            "M354_08_PP_score.krn": ["01", "02", "03", "04"],
            "M354_09_PP_score.krn": ["01", "02", "03", "04"],
            "M354_10_PP_score.krn": ["01", "02", "03", "04"],
            "M354_11_PP_score.krn": ["01", "02", "03", "04"],
            "M354_12_PP_score.krn": ["01", "02", "03", "04", "05", "06"]
        }},
        "K398": {"Krn": {
            "M398_00_PP_score.krn": ["01", "02"],
            "M398_01_PP_score.krn": ["01", "02"],
            "M398_02_PP_score.krn": ["01", "02"],
            "M398_03_PP_score.krn": ["01", "02"],
            "M398_04_PP_score.krn": ["01", "02"],
            "M398_05_PP_score.krn": ["01", "02"],
            "M398_06_PP_score.krn": ["01", "02", "03"]
        }},
        "K455": {"Krn": {
            "M455_00_PP_score.krn": ["01", "02", "03"],
            "M455_01_PP_score.krn": ["01", "02", "03"],
            "M455_02_PP_score.krn": ["01", "02", "03"],
            "M455_03_PP_score.krn": ["01", "02", "03"],
            "M455_04_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "M455_05_PP_score.krn": ["01", "02", "03"],
            "M455_06_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "M455_07_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "M455_08_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M455_09_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "M455_10_PP_score.krn": ["01", "02", "03", "04",
                                      "05", "06", "07", "08"]
        }},
        "K501": {"Krn": {
            "M501_00_PP_score.krn": ["01", "02", "03", "04"],
            "M501_01_PP_score.krn": ["01", "02", "03", "04"],
            "M501_02_PP_score.krn": ["01", "02", "03", "04"],
            "M501_03_PP_score.krn": ["01", "02", "03", "04"],
            "M501_04_PP_score.krn": ["01", "02", "03", "04"],
            "M501_05_PP_score.krn": ["01", "02", "03", "04", "05", "06", "07",
                                      "08", "09", "10", "11", "12", "13", "14"],
        }},
        "K573": {"Krn": {
            "M573_00_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M573_01_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M573_02_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M573_03_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M573_04_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M573_05_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M573_06_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M573_07_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M573_08_PP_score.krn": ["01", "02", "03", "04", "05"],
            "M573_09_PP_score.krn": ["01", "02", "03", "04",
                                      "05", "06", "07", "08"]
        }},
        "K613": {"Krn": {
            "M613_V00_PP_score.krn": ["01", "02", "03", "04"],
            "M613_V01_PP_score.krn": ["01", "02", "03", "04"],
            "M613_V02_PP_score.krn": ["01", "02", "03", "04"],
            "M613_V03_PP_score.krn": ["01", "02", "03", "04"],
            "M613_V04_PP_score.krn": ["01", "02", "03", "04"],
            "M613_V05_PP_score.krn": ["01", "02", "03", "04"],
            "M613_V06_PP_score.krn": ["01", "02", "03", "04"],
            "M613_V07_PP_score.krn": ["01", "02", "03", "04"],
            "M613_V08_PP_score.krn": ["02", "03", "04",
                                       # Warning: notation
                                       "022", "e", "f", "g", "h"],
        }}
    },
    # <Composer folder>: <Piece folder>
    "Beethoven": {
        # <Piece folder>: (<Krn folder>:) <Variation template>
        "B063": {"Krn": { 
            # <Variation template>: [<Phrase id>]
            "B063_00_PP_score.krn": ["01", "02", "03", "04"],
            "B063_01_PP_score.krn": ["01", "02", "03", "04"],
            "B063_02_PP_score.krn": ["01", "02", "03", "04"],
            "B063_03_PP_score.krn": ["01", "02", "03", "04"],
            "B063_04_PP_score.krn": ["01", "02", "03", "04"],
            "B063_05_PP_score.krn": ["01", "02", "03", "04"],
            "B063_06_PP_score.krn": ["01", "02", "03", "04"],
            "B063_07_PP_score.krn": ["01", "02", "03", "04"],
            "B063_08_PP_score.krn": ["01", "02", "03", "04"],
            "B063_09_PP_score.krn": ["01", "02", "03", "04"]
        }},
        "B064": {"Krn": {
            "B064_00_PP_score.krn": ["01", "02", "03"],
            "B064_01_PP_score.krn": ["01", "02", "03"],
            "B064_02_PP_score.krn": ["01", "02", "03"],
            "B064_03_PP_score.krn": ["01", "02", "03"],
            "B064_04_PP_score.krn": ["01", "02", "03"],
            "B064_05_PP_score.krn": ["01", "02", "03"],
            "B064_06_PP_score.krn": ["01", "02", "03"]
        }},
        "B065": {"Krn": {
            "B065_00_PP_score.krn": ["01", "02"],
            "B065_01_PP_score.krn": ["01", "02"],
            "B065_02_PP_score.krn": ["01", "02"],
            "B065_03_PP_score.krn": ["01", "02"],
            "B065_04_PP_score.krn": ["01", "02"],
            "B065_05_PP_score.krn": ["01", "02"],
            "B065_06_PP_score.krn": ["01", "02", "03"],
            "B065_07_PP_score.krn": ["01", "02"],
            "B065_08_PP_score.krn": ["01", "02"],
            "B065_09_PP_score.krn": ["01", "02"],
            "B065_10_PP_score.krn": ["01", "02"],
            "B065_11_PP_score.krn": ["01", "02"],
            "B065_12_PP_score.krn": ["01", "02", "03"],
            "B065_13_PP_score.krn": ["01", "02"],
            "B065_14_PP_score.krn": ["01", "02", "03", "04"],
            "B065_15_PP_score.krn": ["01", "02", "03"],
            "B065_16_PP_score.krn": ["01", "02"],
            "B065_17_PP_score.krn": ["01", "02"],
            "B065_18_PP_score.krn": ["01", "02"],
            "B065_19_PP_score.krn": ["01", "02"],
            "B065_20_PP_score.krn": ["01", "02"],
            "B065_21_PP_score.krn": ["01", "02"],
            "B065_22_PP_score.krn": ["01", "02"],
            "B065_23_PP_score.krn": ["01", "02", "03", "04"],
            "B065_24_PP_score.krn": ["01", "02", "03", "04", "05"]
        }},
        "B066": {"Krn": {
            "B066_00_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B066_01_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B066_02_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B066_03_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B066_04_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B066_05_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B066_06_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B066_07_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B066_08_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B066_09_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B066_10_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B066_11_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B066_12_PP_score.krn": ["01", "02", "03", "04", "05", "06", "07"],
            "B066_13_PP_score.krn": ["01", "02", "03", "04", "05", "06"]
        }},
        "B068": {"Krn": {
            "B068_00_PP_score.krn": ["01", "02", "03", "04"],
            "B068_01_PP_score.krn": ["01", "02", "03", "04"],
            "B068_02_PP_score.krn": ["01", "02", "03", "04"],
            "B068_03_PP_score.krn": ["01", "02", "03", "04"],
            "B068_04_PP_score.krn": ["01", "02", "03", "04"],
            "B068_05_PP_score.krn": ["01", "02", "03", "04"],
            "B068_06_PP_score.krn": ["01", "02", "03", "04"],
            "B068_07_PP_score.krn": ["01", "02", "03", "04"],
            "B068_08_PP_score.krn": ["01", "02", "03", "04"],
            "B068_09_PP_score.krn": ["01", "02", "03", "04"],
            "B068_10_PP_score.krn": ["01", "02", "03", "04"],
            "B068_11_PP_score.krn": ["01", "02", "03", "04"],
            "B068_12_PP_score.krn": ["01", "02", "03", "04",
                                      "05", "06", "07", "08"]
        }},
        "B069": {"Krn": {
            "B069_00_PP_score.krn": ["01", "02", "03"],
            "B069_01_PP_score.krn": ["01", "02", "03"],
            "B069_02_PP_score.krn": ["01", "02", "03"],
            "B069_03_PP_score.krn": ["01", "02", "03"],
            "B069_04_PP_score.krn": ["01", "02", "03"],
            "B069_05_PP_score.krn": ["01", "02", "03"],
            "B069_06_PP_score.krn": ["01", "02", "03"],
            "B069_07_PP_score.krn": ["01", "02", "03"],
            "B069_08_PP_score.krn": ["01", "02", "03"],
            "B069_09_PP_score.krn": ["01", "02", "03", "04", "05", "06"]
        }},
        "B070": {"Krn": {
            "B070_00_PP_score.krn": ["01", "02", "03", "04"],
            "B070_01_PP_score.krn": ["01", "02", "03", "04"],
            "B070_02_PP_score.krn": ["01", "02", "03", "04"],
            "B070_03_PP_score.krn": ["01", "02", "03", "04"],
            "B070_04_PP_score.krn": ["01", "02", "03", "04"],
            "B070_05_PP_score.krn": ["01", "02", "03", "04"],
            "B070_06_PP_score.krn": ["01", "02", "03", "04", "05", "06", "07"]
        }},
        "B071": {"Krn": { # Warning: notation
            "Wo071_V00_PP_score.krn": ["01", "02", "03", "04"],
            "Wo071_V01_PP_score.krn": ["01", "02", "03", "04"],
            "Wo071_V02_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "Wo071_V03_PP_score.krn": ["01", "02", "03", "04"],
            "Wo071_V04_PP_score.krn": ["01", "02", "03", "04"],
            "Wo071_V05_PP_score.krn": ["01", "02", "03", "04"],
            "Wo071_V06_PP_score.krn": ["01", "02", "03", "04"],
            "Wo071_V07_PP_score.krn": ["01", "02", "03", "04"],
            "Wo071_V08_PP_score.krn": ["01", "02", "03", "04"],
            "Wo071_V09_PP_score.krn": ["01", "02", "03", "04"],
            "Wo071_V10_PP_score.krn": ["01", "02", "03", "04"],
            "Wo071_V11_PP_score.krn": ["01", "02", "03", "04"],
            "Wo071_V12_PP_score.krn": ["00", "01", "02", "03", # Warning: notation 00
                                        "04", "05", "06", "07"]
        }},
        "B072": {"Krn": {
            "B072_00_PP_score.krn": ["01", "02", "03", "04"],
            "B072_01_PP_score.krn": ["01", "02", "03", "04"],
            "B072_02_PP_score.krn": ["01", "02", "03", "04"],
            "B072_03_PP_score.krn": ["01", "02", "03", "04"],
            "B072_04_PP_score.krn": ["01", "02", "03", "04"],
            "B072_05_PP_score.krn": ["01", "02", "03", "04"],
            "B072_06_PP_score.krn": ["01", "02", "03", "04"],
            "B072_07_PP_score.krn": ["01", "02", "03", "04"],
            "B072_08_PP_score.krn": ["01", "02", "03", "04", "05",
                                       "06", "07", "08", "09", "10", "11"],
        }},
        "B073": {"Krn": {
            "B073_00_PP_score.krn": ["01", "02"],
            "B073_01_PP_score.krn": ["01", "02", "03"],
            "B073_02_PP_score.krn": ["01", "02"],
            "B073_03_PP_score.krn": ["01", "02"],
            "B073_04_PP_score.krn": ["01", "02"],
            "B073_05_PP_score.krn": ["01", "02"],
            "B073_06_PP_score.krn": ["01", "02"],
            "B073_07_PP_score.krn": ["01", "02"],
            "B073_08_PP_score.krn": ["01", "02", "03"],
            "B073_09_PP_score.krn": ["01", "02"],
            "B073_10_PP_score.krn": ["01", "02", "03", "04",
                                      "05", "06", "07", "08", "09"],
        }},
        "B075": {"Krn": {
            "B075_00_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B075_01_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B075_02_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B075_03_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B075_04_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B075_05_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B075_06_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B075_07_PP_score.krn": ["01", "02", "03", "04", "05", "06", "07"],
        }},
        "B076": {"Krn": {
            "B076_00_PP_score.krn": ["01", "02", "03"],
            "B076_01_PP_score.krn": ["01", "02", "03"],
            "B076_02_PP_score.krn": ["01", "02", "03"],
            "B076_03_PP_score.krn": ["01", "02", "03"],
            "B076_04_PP_score.krn": ["01", "02", "03"],
            "B076_05_PP_score.krn": ["01", "02", "03"],
            "B076_06_PP_score.krn": ["01", "02", "03"],
            "B076_07_PP_score.krn": ["01", "02", "03"],
            "B076_08_PP_score.krn": ["01", "02", "03", "04", "05", "06"]
        }},
        "B077": {"Krn": {
            "B077_00_PP_score.krn": ["01", "02", "03", "04"],
            "B077_01_PP_score.krn": ["01", "02", "03", "04"],
            "B077_02_PP_score.krn": ["01", "02", "03", "04"],
            "B077_03_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "B077_04_PP_score.krn": ["01", "02", "03", "04"],
            "B077_05_PP_score.krn": ["01", "02", "03", "04"],
            "B077_06_PP_score.krn": ["01", "02", "03", "04", "05", "06", "07"]
        }},
        "B078": {"Krn": {
            "B078_00_PP_score.krn": ["01", "02"],
            "B078_01_PP_score.krn": ["01", "02"],
            "B078_02_PP_score.krn": ["01", "02"],
            "B078_03_PP_score.krn": ["01", "02"],
            "B078_04_PP_score.krn": ["01", "02"],
            "B078_05_PP_score.krn": ["01", "02"],
            "B078_06_PP_score.krn": ["01", "02"],
            "B078_07_PP_score.krn": ["01", "02", "03", "04", #"05", # Warning: 05 is missing
                                      "06", "07", "08", "09"],
        }},
        "B080": {"Krn": {
            "B080_00_PP_score.krn": ["01"], "B080_01_PP_score.krn": ["01"],
            "B080_02_PP_score.krn": ["01"], "B080_03_PP_score.krn": ["01"],
            "B080_04_PP_score.krn": ["01"], "B080_05_PP_score.krn": ["01"],
            "B080_06_PP_score.krn": ["01"], "B080_07_PP_score.krn": ["01"],
            "B080_08_PP_score.krn": ["01"], "B080_09_PP_score.krn": ["01"],
            "B080_10_PP_score.krn": ["01"], "B080_11_PP_score.krn": ["01"],
            "B080_12_PP_score.krn": ["01"], "B080_13_PP_score.krn": ["01"],
            "B080_14_PP_score.krn": ["01"], "B080_15_PP_score.krn": ["01"],
            "B080_16_PP_score.krn": ["01"], "B080_17_PP_score.krn": ["01"],
            "B080_18_PP_score.krn": ["01"], "B080_19_PP_score.krn": ["01"],
            "B080_20_PP_score.krn": ["01"], "B080_21_PP_score.krn": ["01"],
            "B080_22_PP_score.krn": ["01"], "B080_23_PP_score.krn": ["01"],
            "B080_24_PP_score.krn": ["01"], "B080_25_PP_score.krn": ["01"],
            "B080_26_PP_score.krn": ["01"], "B080_27_PP_score.krn": ["01"],
            "B080_28_PP_score.krn": ["01"], "B080_29_PP_score.krn": ["01"],
            "B080_30_PP_score.krn": ["01"], "B080_31_PP_score.krn": ["01"],
            "B080_32_PP_score.krn": ["01", "02"],
        }},
        "Opus34": {"Krn": {
            "Bo34_00_PP_score.krn": ["01", "02", "03", "04", "05"],
            "Bo34_01_PP_score.krn": ["01", "02", "03", "04", "05"],
            "Bo34_02_PP_score.krn": ["01", "02", "03", "04", "05"],
            "Bo34_03_PP_score.krn": ["01", "02", "03", "04", "05"],
            "Bo34_04_PP_score.krn": ["01", "02", "03", "04", "05"],
            "Bo34_05_PP_score.krn": ["01", "02", "03", "04", "05", "06"],
            "Bo34_06_PP_score.krn": ["01", "02", "03", "04", "05",
                                      "06", "07", "08", "09", "10"]
        }},
        "Opus76": {"Krn": {
            "Bo76_00_PP_score.krn": ["01", "02"],
            "Bo76_01_PP_score.krn": ["01", "02"],
            "Bo76_02_PP_score.krn": ["01", "02"],
            "Bo76_03_PP_score.krn": ["01", "02"],
            "Bo76_04_PP_score.krn": ["01", "02"],
            "Bo76_05_PP_score.krn": ["01", "02"],
            "Bo76_06_PP_score.krn": ["01", "02", "03", "04",
                                      "05", "06", "07", "08"]
        }}
    }
}



####
# Utils
####

REGEX_PHRASE_NAME = "^([a-zA-Z]{1,3})_?([KWoOp]{0,3})\.?(\d{2,3})_V?(\d+)_([\defgh]+)(.*)$"
def standardize(name: str, shorten=True):
    """
    Return given a phrase name into standardized naming convention: 

    'C_XXX_VV_PP' with
    - C: the composer: M for Mozart, B for Beethoven
    - XXX: index of the piece (Köchel, opus, or WoO), with prefix and 3 digits
    - VV: index of the variation (2 digits)
    - PP: index of the phrase (2 digits)

    Parameters
    ----------
    name: str
        Filename to standardize.
    shorten: bool
        If True, returns the filename in the form `<composer><piece>
        _<variation>_<phrase>`, without full path, nor file extension.
        Otherwise, details all provided information, including path,
        file extension (.krn, .tsv...), and additional suffixes.
        Defaults to True.

    Returns
    -------
    str

    Examples
    --------
    'M573_02_03' -> 'M_K.573_02_03'
    'M613_V00_01' -> 'M_K.613_00_01'
    'Bo34_06_01' -> 'B_op.034_06_01'
    'Wo071_V00_01.krn' -> 'B_WoO.071_00_01.krn'
    './path/to/Wo071_V00_01_special.tsv' -> './path/to/B_WoO.071_00_01_special.tsv'
    """
    directory = os.path.dirname(name)
    basename = os.path.basename(name)
    regex_match = re.match(REGEX_PHRASE_NAME, basename)
    if not regex_match:
        raise RuntimeError(f"Found no match with file: {basename}")
    composer_str = regex_match.group(1)
    piece_prefix_str = regex_match.group(2)
    piece_str = regex_match.group(3)
    variation_str = regex_match.group(4)
    phrase_str = regex_match.group(5)
    other = regex_match.group(6)

    if composer_str in ["M", "K"]:
        composer = "M" # Mozart
        piece_prefix = "K"
    elif composer_str in ["B", "Bo", "Wo", "Opus"]:
        composer = "B" # Beethoven
        if composer_str in ["Bo", "Opus"]:
            piece_prefix = "op"
        else:
            piece_prefix = "WoO"
    else:
        raise RuntimeError(f"Unknown composer string in file: {basename}")
    if piece_prefix_str:
        # Overwrite piece_prefix information that would have merged with composer info
        piece_prefix = piece_prefix_str

    # Add leading '0' if needed
    piece = piece_prefix + '.' + piece_str.zfill(3)
    variation = variation_str.zfill(2)
    phrase = phrase_str.zfill(2)

    # Build standardized string
    if shorten:
        res = f"{composer}_{piece}_{variation}_{phrase}"
    else:
        res = os.path.join(directory, f"{composer}_{piece}_{variation}_{phrase}{other}")
    return res

def get_composer(name: str, is_standardized=False):
    """
    Return the name of the composer, retrieved from the full name:
    Mozart or Beethoven.

    Parameters
    ----------
    name: str
        Target phrase name.
    is_standardized: bool
        Indicates that given name is already in its standard form.
        Defaults to False.

    Returns
    -------
    str
    """
    standard_name = name if is_standardized else standardize(name)
    composer_str = os.path.basename(standard_name)[0]
    if composer_str == "M":
        res = "Mozart"
    elif composer_str == "B":
        res = "Beethoven"
    else:
        raise RuntimeError(f"Unknown composer id in {name}")
    return res

def get_piece(name: str, is_standardized=False):
    """
    Return the name of the piece, retrieved from a full name.
    (e.g. K.573, op.034, WoO.071)

    Parameters
    ----------
    name: str
        Target phrase name.
    is_standardized: bool
        Indicates that given name is already in its standard form.
        Defaults to False.

    Returns
    -------
    str
    """
    standard_name = name if is_standardized else standardize(name)
    return os.path.basename(standard_name).split('_')[1]

def get_piece_number(name: str, is_standardized=False):
    """
    Return the number of the piece, retrieved from a full name.
    (e.g. `get_piece_number('B_op.034_06_01') -> 34`)

    Parameters
    ----------
    name: str
        Target phrase name.
    is_standardized: bool
        Indicates that given name is already in its standard form.
        Defaults to False.

    Returns
    -------
    str
    """
    standard_name = name if is_standardized else standardize(name)
    raw_piece = standard_name.split('_')[1].split('.')[-1]
    res = raw_piece.lstrip('0') # Remove leading '0'
    return res

def get_variation(name: str, is_standardized=False, to_int=False):
    """
    Return the number of the variation, retrieved from a full name.
    (e.g. `get_variation('B_op.034_06_01') -> '6'`)

    Parameters
    ----------
    name: str
        Target phrase name.
    is_standardized: bool
        Indicates that given name is already in its standard form.
        Defaults to False.
    to_int:
        If True, return variation number as an integer. Otherwise,
        return it as a string. Defaults to False (returns a string).

    Returns
    -------
    int if to_int is True; otherwise, str
    """
    standard_name = name if is_standardized else standardize(name)
    raw_variation = standard_name.split('_')[2]
    if to_int:
        res = int(raw_variation)
    elif raw_variation in ["", "0", "00"]:
        res = "Thema"
    else:
        res = raw_variation.lstrip('0') # Remove leading '0'
    return res

def get_phrase(name: str):
    """
    Return the number of the phrase, retrieved from a full name.
    (e.g. `get_variation('B_op.034_06_01') -> 1`)

    Parameters
    ----------
    name: str
        Target phrase name.
    is_standardized: bool
        Indicates that given name is already in its standard form.
        Defaults to False.

    Returns
    -------
    int
    """
    standard_name = standardize(name)
    return standard_name.split('_')[3].lstrip('0') # Remove leading '0'



####
# Paths retrieval
####

def get_tavern_path_list(
        target=None,
        dataset_dir=TAVERN_DIR,
        file_architecture=TAVERN_ARCHITECTURE
        ):
    """
    Return the list of path for all phrase files (in **kern format)
    of the TAVERN dataset.

    Parameters
    ----------
    target: str (optional)
        If not None, only get path with given target in their name.
        For example, target="K455" will only retrieves phrases from
        Mozart variations in piece K.455. Defaults to None.
    dataset_dir: str or Path object
        Local relative path to a clone of original TAVERN repository.
    file_architecture: dict
        Detailed architecture of folders and files in the TAVERN repository.

    Returns
    -------
    tavern_path_list: list of [str or Path object]
    """
    tavern_path_list = []
    # Each composer
    for composer, piece_dict in file_architecture.items():
        # Each piece
        for piece, krn_folder in piece_dict.items():
            for krn_dir, krn_data in krn_folder.items():
                # Each variation
                for variation_template, phrases in krn_data.items():
                    # Each phrase
                    for phrase_id in phrases:
                        full_path = os.path.join(
                            dataset_dir,
                            composer,
                            piece,
                            krn_dir,
                            variation_template.replace("PP", phrase_id)
                        )
                        if target:
                            # Retrieve the path only if it is targeted
                            if target in full_path:
                                tavern_path_list.append(full_path)
                        else:
                            # Otherwise, add all
                            tavern_path_list.append(full_path)
    return tavern_path_list

def get_tavern_notes_path(
        target=None,
        dataset_dir=TAVERN_NOTES_DIR,
        file_architecture=TAVERN_ARCHITECTURE
        ):
    """
    Return the list of path for all files of note dictionaries, in TSV
    (Tab-Separated Values) format for each phrase of the TAVERN dataset.

    Parameters
    ----------
    target: str (optional)
        If not None, only get path with given target in their name.
        For example, target="K455" will only retrieves phrases from
        Mozart variations in piece K.455. Defaults to None.
    dataset_dir: str or Path object
        Local relative path to a clone of original TAVERN repository.
    file_architecture: dict
        Detailed architecture of folders and files in the TAVERN repository.

    Returns
    -------
    path_list: list of [str or Path object]
        List of paths in standardized format, e.g. 'M_K.613_08_03_notes.tsv'.
    """
    # Get all files reference in standard format: 'M_K.613_08_03'
    standardized_tavern_files = [
        standardize(f, shorten=True)
        for f in get_tavern_path_list(
            target=target,
            dataset_dir=dataset_dir,
            file_architecture=file_architecture
        )
    ]
    # Build new paths
    path_list = [
        os.path.join(dataset_dir, f"{phrase_name}_notes.tsv")
        for phrase_name in standardized_tavern_files
    ]
    return path_list

def get_tavern_desc_path(
        target=None,
        dataset_dir=TAVERN_DESC_DIR,
        file_architecture=TAVERN_ARCHITECTURE
        ):
    """
    Return the list of path for all files of descriptors, in TSV
    (Tab-Separated Values) format for each phrase of the TAVERN dataset.

    Parameters
    ----------
    target: str (optional)
        If not None, only get path with given target in their name.
        For example, target="K455" will only retrieves phrases from
        Mozart variations in piece K.455. Defaults to None.
    dataset_dir: str or Path object
        Local relative path to a clone of original TAVERN repository.
    file_architecture: dict
        Detailed architecture of folders and files in the TAVERN repository.

    Returns
    -------
    path_list: list of [str or Path object]
        List of paths in standardized format, e.g. 'path/to/M_K.613_08_03_desc.tsv'.
    """
    # Get all files reference in standard format: 'M_K.613_08_03'
    standardized_tavern_files = [
        standardize(f, shorten=True)
        for f in get_tavern_path_list(
            target=target,
            dataset_dir=dataset_dir,
            file_architecture=file_architecture
        )
    ]
    # Build new paths
    path_list = [
        os.path.join(dataset_dir, f"{phrase_name}_desc.tsv")
        for phrase_name in standardized_tavern_files
    ]
    return path_list



####
# Tests
####

def test_naming():
    """
    
    """
    pathlist = get_tavern_path_list()
    for path in pathlist:
        print(
            f"{standardize(path)}, "\
            f"by {get_composer(path)}, "\
            f"piece {get_piece_number(path)}, "\
            f"variation {get_variation(path)}, "\
            f"phrase {get_phrase(path)}."
        )



####
# Main functions
####

def check_access(path_list, verbose=True):
    """
    Check that all filenames in filelist exist in
    local file architecture.

    Parameters
    ----------
    path_list: list of [str or Path object]
        List of path to files to be checked.
    verbose: bool
        If True, the function display all filenames
        in filelist that are not found. Defaults to True.

    Returns
    -------
    bool
        Return True if all files were found. Otherwise, False.
    """
    nb_success = 0
    for f in path_list:
        if os.path.exists(f):
            nb_success += 1
        elif verbose:
            print(f"Not found: {f}")
    if verbose:
        print(f"Found {nb_success} on {len(path_list)} files.")
    return nb_success == len(path_list)

def check_parsing(krn_list, verbose=True):
    """
    Check that all krn files in given list can be read by 
    music21 parser.

    Parameters
    ----------
    path_list: list of [str or Path object]
        List of path to .krn files to be checked.
    verbose: bool
        If True, the function display all filenames
        in filelist that are not found, or that cannot
        be parsed successfully. Defaults to True.

    Return
    ------
    bool
        Return True if all files can be parsed successfully. Otherwise, False.
    """
    if verbose and not check_access(krn_list, verbose=verbose):
        print("WARNING: files mentionned above cannot be tested because they \
               were not found in local file architecture.")
    
    nb_success = 0
    for krn_file in krn_list:
        if verbose:
            print(f"Parsing {krn_file}", end="\r")
        try:
            stream = m21.converter.parse(krn_file)
        except Exception as e:
            if verbose:
                print(f"Failed to parse {krn_file} with error: {e}")
        else:
            # No error
            nb_success += 1

    if verbose:
        print(f"\nSuccessfully parsed {nb_success} on {len(krn_list)} files.")
    return nb_success == len(krn_list)

def convert_to_note_tsv(
        path_list,
        output_dir=TAVERN_NOTES_DIR,
        replace=None, # with warning
        verbose=True,
        skip_cadenza=True
        ):
    """
    Convert each .krn file in given list to TSV files (Tab-separated-values)
    that represent the information of each notes on separate lines.

    Parameters
    ----------
    path_list: list of [str or Path object]
        List of path to .krn files to be converted.
    output_dir: str or Path object
        Path to the folder in which output TSV files will be saved.
    replace: bool
        If True, erase previous files with the same name. If False,
        skip the file. If None, ask the user. Defaults to None.
    verbose: bool
        If True, the function display all filenames
        in filelist that are not found, or that cannot
        be parsed successfully. Defaults to True.
    skip_cadenza: bool, optional
        If True, ignore unmetered measures like cadenza. Defaults to True.

    Returns
    -------
    bool
        True if all conversions were successful.
    """
    nb_success = 0
    nb_skipped = 0
    for file in path_list:
        if verbose:
            print("Converting", file, end='\r')
        output_path = os.path.join(output_dir, standardize(file)+"_notes.tsv")

        if replace is None and os.path.exists(output_path):
            replace_input = input(f"\n\nWARNING: {output_path} already exists.\
                                    \nAre you sure that you want to replace its content ?\
                                    \nThis choice will apply to other files. ([y]/n) ")
            replace = replace_input.lower() in ["yes", "y", ""]
        if not replace and os.path.exists(output_path):
            # Already an existing file: skip
            nb_skipped += 1
            continue

        try:
            piece_to_note_tsv(
                file,
                output_name=output_path,
                skip_cadenza=skip_cadenza # Skip unmetered bars
            )
        except Exception as e:
            print(f"Conversion failed for {file} with error: {e}")
        else:
            nb_success += 1
    if verbose:
        print(f"\nSuccessfully converted {nb_success} on {len(path_list) - nb_skipped} files.")
    return nb_success == len(path_list)

def convert_to_desc_tsv(
        path_list,
        output_dir=TAVERN_DESC_DIR,
        replace=None, # with warning
        whole_fractions_convention=False,#TODO: to check
        verbose=True):
    """
    Convert each notedict .tsv file in path_list to descriptor TSV (Tab-separated-values).

    whole_fractions_convention : bool
        If True, parsed onsets and durations are encoded as fractions
        of whole notes (1/4 for quarter notes), as in Annotated Mozart
        Sonatas. Else, onsets and durations are in quarter lentgh (1.0
        for quarter notes). Defaults to False (default for generate_notes() outputs).
    #TODO
    """
    nb_success = 0
    nb_skipped = 0
    for file in path_list:
        if verbose:
            print("Converting", file, end='\r')
        output_path = os.path.join(output_dir, standardize(file)+"_desc.tsv")

        if replace is None and os.path.exists(output_path):
            replace_input = input(f"\n\nWARNING: {output_path} already exists.\
                                    \nAre you sure that you want to replace its content ?\
                                    \nThis choice will apply to other files. ([y]/n) ")
            replace = replace_input.lower() in ["yes", "y", ""]
        if not replace and os.path.exists(output_path):
            # Already an existing file: skip
            nb_skipped += 1
            continue

        try:
            note_list = extract_notes(file)
            extract_descriptors(
                note_list=note_list,
                output_file=output_path,
                verbose=verbose,
                whole_fractions_convention=whole_fractions_convention
            )
        except Exception as e:
            print(f"Conversion failed for {file} with error: {e}")
        else:
            nb_success += 1
    if verbose:
        print(f"\nSuccessfully converted {nb_success} on {len(path_list) - nb_skipped} files.")
    return nb_success == len(path_list)



if __name__ == "__main__":
    # Adapt TAVERN_DIR, in the global variables, to your local architecture

    # Uncomment to run
    ## Step 1: Check that the files exist
    check_access(get_tavern_path_list())

    ## Step 2: Check that the files can be parsed
    #check_parsing(get_tavern_path_list())

    ## Step 3: Convert .krn to TSV files of notes
    #test_naming()
    #convert_to_note_tsv(get_tavern_path_list())
    #check_access(get_tavern_notes_path()) # Check creation and access of these files

    ## Step 4: Convert note-dict TSV to descriptor TSV
    #convert_to_desc_tsv(get_tavern_notes_path(TAVERN_NOTES_DIR))
    #check_access(get_tavern_desc_path()) # Check creation and access of these files
