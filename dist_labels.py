"""
License
-------
This file is part of "Comparing Texture" http://algomus.fr/code 
Copyright (C) 2021-2023 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Comparing Texture" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Comparing Texture" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Comparing Texture". If not, see
http://www.gnu.org/licenses/
"""
import os
from typing import List, Any, Union, Tuple

import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np

from dist_base import hamming, minkowski
from textureclass import Texture
from tsv_converter import extract_labels

####
# Distance
####
DEFAULT_TEXTURAL_ELEMENTS = [
    'M', 'H', 'S', # layer functions: melodic, harmonic, static
    'h+', 'p+', 'o', # synchronicity, parallelism: homorhythmy, parallel and octave motions
    's', 't', 'b', 'r', # characteristic figures: scale motives, sustained notes, oscillations, repeated notes
    '_', ',' # sparsity, sequentiality (two distinct textures in the measure)
]
def texel_distance(
        t1: Union[str, Texture],
        t2: Union[str, Texture],
        textural_elements: List[str] = DEFAULT_TEXTURAL_ELEMENTS,
        verbose: bool = False):
    """
    Compute Hamming distance between textural elements of
    given texture annotations.

    Parameters
    ----------
    t1: str or Texture object
        Textural annotation to be compared.
    t2: str or Texture object
        Other textural annotation to be compared with the first one.
    textural_elements: list of str, optional
        List of all textural elements to take into account for comparison.
    verbose : bool, optional

    Returns
    -------
    float
    """
    # Type check
    if type(t1) is str:
        t1 = Texture(t1)
    elif type(t1) is not Texture:
        raise TypeError("Invalid type for argument 't1'. Accepted types are string and Texture object.")
    if type(t2) is str:
        t2 = Texture(t2)
    elif type(t2) is not Texture:
        raise TypeError("Invalid type for argument 't2'. Accepted type are string and Texture object.")
    
    # Extract textural elements
    textural_elements1 = []
    textural_elements2 = []
    for texture, values in zip([t1, t2], [textural_elements1, textural_elements2]):
        if 'M' in textural_elements:
            values.append(int(texture.has_melodic()))
        if 'H' in textural_elements:
            values.append(int(texture.has_harmonic()))
        if 'S' in textural_elements:
            values.append(int(texture.has_static()))
        if 'h' in textural_elements:
            values.append(int(texture.has_attribute('h')))
        if 'p' in textural_elements:
            values.append(int(texture.has_attribute('p')))
        if 'o' in textural_elements:
            values.append(int(texture.has_attribute('o')))
        if 'h+' in textural_elements:
            values.append(int(texture.has_homorhythmy()))
        if 'p+' in textural_elements:
            values.append(int(texture.has_parallel_motions()) )
        if 's' in textural_elements:
            values.append(int(texture.has_attribute('s')))
        if 't' in textural_elements:
            values.append(int(texture.has_attribute('t')))
        if 'b' in textural_elements:
            values.append(int(texture.has_attribute('b')))
        if 'r' in textural_elements:
            values.append(int(texture.has_attribute('r')))
        if '_' in textural_elements:
            values.append(int(texture.has_attribute('_')))
        if ',' in textural_elements:
            values.append(int(texture.is_sequential()))
    
    # Compute distance
    if verbose:
        print(f"{textural_elements}:\n{textural_elements1}\n{textural_elements2}")
    return hamming(textural_elements1, textural_elements2)


def dd_distance(
        t1: Union[str, Texture],
        t2: Union[str, Texture],
        ord: int = 2,
        verbose: bool = False):
    """
    Return Minkoswky distance between (density, diversity) tuples,
    extracted from given texture annotations.

    Parameters
    ----------
    t1: str or Texture object
        Textural annotation to be compared.
    t2: str or Texture object
        Other textural annotation to be compared with the first one.
    ord: {positive int, inf}, optional
        Order of the computed distance. In particular, use 1 for Manhattan
        distance and 2 for Euclidean distance. Defaults to 2.
    sequential_avg: bool, optional
        If True, extract the density values as the average of density values from
        both sequential textural configuration in single texture labels "t_A, t_B".
        Otherwise, only extract the density of the first textural configuation.
        Defaults to True.
    
    Returns
    -------
    float
    """
    # Type check
    if type(t1) is str:
        t1 = Texture(t1)
    elif type(t1) is not Texture:
        raise TypeError("Invalid type for argument 't1'. Accepted types are string and Texture object.")
    if type(t2) is str:
        t2 = Texture(t2)
    elif type(t2) is not Texture:
        raise TypeError("Invalid type for argument 't2'. Accepted types are string and Texture object.")
    
    # Extract density and diversity
    if t1.is_sequential():
        # Build global density and diversity value for the whole measure
        # Default: take the maximum values in the two textural configurations
        dens_div1 = (
            max(int(t1), int(t1.sequential)), # density
            max(len(t1), len(t1.sequential))  # diversity
        )
    else:
        dens_div1 = (int(t1), len(t1)) # density, diversity
    if t2.is_sequential():
        dens_div2 = (
            max(int(t2), int(t2.sequential)), # density
            max(len(t2), len(t2.sequential))  # diversity
        )
    else:
        dens_div2 = (int(t2), len(t2))

    # Compute distance
    if verbose:
        print(f"(density, diversity): {dens_div1} and {dens_div2}")
    return minkowski(np.array(dens_div1), np.array(dens_div2), ord=ord)
