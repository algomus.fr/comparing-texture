"""
Evaluation tools for textural distance, on the TAVERN dataset.


License
-------
This file is part of "Comparing Texture" http://algomus.fr/code 
Copyright (C) 2021-2023 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Comparing Texture" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Comparing Texture" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Comparing Texture". If not, see
http://www.gnu.org/licenses/
"""
import csv
import os
from typing import List, Any

import numpy as np
import matplotlib.pyplot as plt

from dataset_tavern import get_tavern_desc_path, standardize
from dist_base import minkowski
from extract_descriptors import extract_descriptor_array


def get_phrases_descriptors(
        phrases_path_list: List[str],
        descriptor_names: List[str] = ["n_onsets", "regularity_std", "n_voices_per_beat_avg", "n_voices_per_onset_std"],
        weights: List[float] = None
    ) -> List[List[np.ndarray]]:
    """
    Parameters
    ----------
    phrases_path_list: str, path-like
        list of path to descriptors TSV files
    descriptor_names : list of str
        Name of the descriptors to be retrieved from target file.
    weights: list of float, optional
        Weight for each of the descriptors in descriptor_names.
        By default, all descriptors have the same weight.

    Returns
    -------
    phrases: List[np.ndarray]
        List of phrases in the corpus, where each phrase is represented by an
        np.ndarray containing one line for each of its bars, and one column
        for each of the selected descriptors.
    """
    phrases = [] # List of all the phrases
    for phrase_path in phrases_path_list:
        phrases.append(
            extract_descriptor_array(
                phrase_path,
                descriptor_names=descriptor_names,
                weights=weights
            )
        )
    return phrases

    

def evaluate_descriptor_distance(
        phrases_path_list: List[str],
        descriptor_names: List[str] = ["n_onsets", "regularity_std", "n_voices_per_beat_avg", "n_voices_per_onset_std"],
        weights: List[float] = None,
        ord=1,
        ignore_contrast_to="self", # "self", "var" or None
        verbose=False
    ) -> float:
    """
    Parameters
    ----------
    phrases_path_list: str, path-like
        list of path to descriptors TSV files
    descriptor_names: list of str
        Name of the descriptors to be retrieved from target file.
    weights: list of float, optional
        Weight for each of the descriptors in descriptor_names.
        By default, all descriptors have the same weight.
    ord: {positive int, inf}, optional
        Order of the norm used for distance computation:
        2 for Euclidian distance, 1 for Manhattan distance. Defaults to 1.
    ignore_contrast_to: str in {"var", "self", None}, optional
        If "self", do not compare phrases to themselves (in 'contrast' computation).
        If "var", do not compare to the phrases in within the same whole variation.
        Defaults to "var".
    verbose: bool, optional
        If True, print additional information during runtime.

    Returns
    -------
    float
        Evaluation metric of the distance. It corresponds to the average inverse specificity
        of phrases in the corpus, which is the ratio between 1. the average distance
        between a pair of measures (m1, mi) where m1 is in P1 and mi in any other
        phrase of the corpus, and 2. the average distance between two measures within the
        same phrase P1. A lower value means a better quality for the textural distance.
    """
    phrases = get_phrases_descriptors(
        phrases_path_list=phrases_path_list,
        descriptor_names=descriptor_names,
        weights=weights
    )
    phrases_id = [
        standardize(phrase_path, shorten=True)
        for phrase_path in phrases_path_list
    ]

    phrase_inverse_specificities = []
    for p1, id_p1 in zip(phrases, phrases_id):
        # Heterogeneity of the phrase:
        # = empirical mean absolute difference for all the measure of the phrase p1
        total_distance = 0
        count_comparison = 0
        for i_m1, m1 in enumerate(p1): 
            for i_m2, m2 in enumerate(p1[i_m1+1:]):
                total_distance += minkowski(m1, m2, ord=ord)
                count_comparison += 1
        if not count_comparison:
            continue
        phrase_heterogeneity = total_distance / count_comparison

        # Contrast between this phrase and other phrases of the corpus
        # = average distance between pairs of measure (m1,m2) in P1 x P2
        total_distance = 0
        count_comparison = 0
        for p2, id_p2 in zip(phrases, phrases_id):
            if ignore_contrast_to=="var" and id_p2[:-3] == id_p1[:-3]:
                # Ignore comparison to phrases in the same variation
                continue
            elif ignore_contrast_to=="self" and id_p2 == id_p1:
                # Ignore comparison to the very same phrase
                continue
            for i_m1, m1 in enumerate(p1):
                for i_m2, m2 in enumerate(p2):
                    total_distance += minkowski(m1, m2, ord=ord)
                    count_comparison += 1
        if not count_comparison:
            continue
        phrase_contrast = total_distance / count_comparison

        # Phrase inverse specificity:
        inverse_specificity = phrase_heterogeneity / phrase_contrast
        phrase_inverse_specificities.append(inverse_specificity)
        if verbose >= 2:
            print(f"Phrase {id_p1}: {inverse_specificity} ({len(p1)} bars)")

    if verbose:
        print(f"Parameters: distance order {ord} on {descriptor_names} with weights {weights}.")
        #print("Final evaluation (inverse specificities of phrases):")
        print(f"Average: {np.average(phrase_inverse_specificities)}")
        print(f"Std: {np.std(phrase_inverse_specificities)}")
        print(f"Minimum: {min(phrase_inverse_specificities)}")
        print(f"First quartile: {np.percentile(phrase_inverse_specificities, 25)}")
        print(f"Median: {np.median(phrase_inverse_specificities)}")
        print(f"Third quartile: {np.percentile(phrase_inverse_specificities, 75)}")
        print(f"Maximum: {max(phrase_inverse_specificities)}")

    # Final value: dictionary of phrases inverse specifities
    return {
        phrase_id:inverse_specificites
        for phrase_id, inverse_specificites in zip(phrases_id, phrase_inverse_specificities)
    }

def plot_stats(
        values,
        style="histogram" # / "barplot"
        ):
    sorted_values = sorted(values)
    q1 = np.percentile(values, 25)
    med = np.median(values)
    q3 = np.percentile(values, 75)
    max_value = max(values)
    x = list(range(len(values)))

    if style.lower() in ["barplot", "bar", "b"]:
        plt.bar(
            x, sorted_values, align='edge',
            alpha=0.2, color='black')
        xmax = len(values)
    elif style.lower() in ["histogram", "histo", "h"]:
        n, _, _ = plt.hist(
            sorted_values, bins=np.linspace(0, max_value, 30),
            orientation="horizontal", alpha=0.2, color='black')
        xmax = max(n) + 1
    else:
        raise ValueError(f"Unknown style argument: {style}. The value must be in ['histogram', 'barplot'].")

    # Horizontal lines for main statistics
    plt.hlines(
        np.average(values), xmin=0, xmax=xmax,
        linestyles='dashed', colors='lightgray')
    plt.hlines(
        med, xmin=0, xmax=xmax,
        linestyles='solid', colors='black')
    plt.hlines(
        q1, xmin=0, xmax=xmax,
        linestyles='solid', colors='gray')
    plt.hlines(
        q3, xmin=0, xmax=xmax,
        linestyles='solid', colors='gray')

    plt.yticks(
        [0, q1, med, q3, 1, max_value],
        [0, f"Q1: {q1:.2f}", f"med: {med:.2f}", f"Q3: {q3:.2f}", "1", f"{max_value:.2f}"]
    )
    plt.xticks([])
    plt.title("Relative-Heterogeneity in phrases of K455 (demo)")
    plt.show()


if __name__ == "__main__":
    # Detailed pipeline and demo coming very soon
    """
    TAVERN_DESCRIPTORS = get_tavern_desc_path()

    results = evaluate_descriptor_distance(
        TAVERN_DESCRIPTORS,
        descriptor_names=["n_onsets", "regularity_std", "n_voices_per_beat_avg", "n_voices_per_onset_std"],
        weights=None,
        ord=1, # Order of the Minkowsky distance: 1 for Manhattan, 2 for euclidean
        verbose=2
    )

    # Uncomment for stats plotting
    plot_stats(list(results.values()))
    """
    pass
