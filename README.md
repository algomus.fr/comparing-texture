[![GNU GPL v3 license](https://img.shields.io/badge/license-GPLv3-green)](http://www.gnu.org/licenses/)

# Comparing texture in piano scores

This repository contains tools used in `Louis Couturier, Louis Bigo, & Florence Levé, 2023. Comparing Texture in Piano Scores. Proceedings of the 24th International Society for Music Information Retrieval Conference.`

## Dependencies

This code has been implemented using Python 3.10.1 and following libraries:
```
matplotlib=3.5.1
music21=7.3.1
numpy=1.22.2
scipy=1.8.0
```

They can be installed using `pip`.
```
pip install <library>=<version>
```

The experiments in this repository requires to clone three datasets in your root folder:
- The TAVERN dataset (Devaney et al. 2015), available at https://github.com/jcdevaney/TAVERN
- The Annotated Mozart Sonatas (Hentschel et al. 2021): https://github.com/DCMLab/mozart_piano_sonatas/tree/main
- The Symbolic Texture dataset (Couturier et al. 2022b): https://gitlab.com/algomus.fr/symbolic-texture-dataset

## Architecture

*Detailed demonstration coming soon*

## Using the repo

*Detailed demonstration coming soon*

## License

The code is licensed under the GNU General Public License v3.0 or any later version. Any rights in individual contents of the database are licensed under the Database Contents License.


## Authors and acknowledgement

Louis Couturier (MIS, Université de Picardie Jules Verne, France), Florence Levé (MIS, UPJV and CRIStAL, UMR 9189 CNRS, Université de Lille) and Louis Bigo (Université de Bordeaux, SCRIME)

Contact: louis.couturier \[at\] u-picardie \[dot\] fr

Feel free to raise an issue if you encounter any unwanted behavior, or to share your suggestions.
