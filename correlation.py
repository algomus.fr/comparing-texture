"""
Tools for correlation between distance values on all
movements from Mozart Sonatas K279, K280 and K283.


License
-------
This file is part of "Comparing Texture" http://algomus.fr/code 
Copyright (C) 2021-2023 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Comparing Texture" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Comparing Texture" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Comparing Texture". If not, see
http://www.gnu.org/licenses/
"""
import numpy as np
import os
import matplotlib.pyplot as plt
from matplotlib import cm


import scipy.stats



def compute_correlation(
        file_1, file_2,
        correlation="spearman" #or "pearson"
        ):
    """
    Compute the correlation between sequences contained in
    two given files.

    Parameters
    ----------
    file_1: str
        Path to .npy file, containing an array.
    file_2: str
        Path to .npy file, containing an array of the same shape.
    correlation: str in {"spearman", "pearson"}
        Type of correlation to compute.

    Returns
    -------
    float between -1 and 1
        value of correlation
    """
    if correlation.lower() in ["spearman", "s"]:
        correlation_func = scipy.stats.spearmanr
    elif correlation.lower() in ["pearson", "p"]:
        correlation_func = scipy.stats.pearsonr
    else:
        raise ValueError(f"Unknown correlation type: {correlation}")    

    with open(file_1, 'rb') as f:
        data_1 = np.load(f)
    with open(file_2, 'rb') as f:
        data_2 = np.load(f)

    res, _ = correlation_func(data_1, data_2) # second output is the p-value
    return res

def all_correlation(file_list):
    """Display correlation between all given data."""
    correlation_spearman = []
    correlation_pearson = []
    for i in range(len(file_list)):
        print(f"Analyze {file_list[i]}")
        for j in range(len(file_list)):
            correlation_spearman.append(compute_correlation(
                file_list[i], file_list[j],
                correlation="spearman"
            ))
            correlation_pearson.append(compute_correlation(
                file_list[i], file_list[j],
                correlation="pearson"
            ))
    print("Spearman's correlation:")
    for i in range(len(file_list)):
        for j in range(len(file_list)):
            print("{:.3f}".format(correlation_spearman[i*len(file_list)+j]), end='  ')
        print()

    print("Pearson's correlation:")
    for i in range(len(file_list)):
        for j in range(len(file_list)):
            print("{:.3f}".format(correlation_pearson[i*len(file_list)+j]), end='  ')
        print()



if __name__ == "__main__":
    # Clean demo coming soon
    """
    input_dir = "npy"
    test_files = [
        os.path.join(input_dir, "distance_4desc.npy"),
        os.path.join(input_dir, "distance_2desc_horiz.npy"),
        os.path.join(input_dir, "distance_2desc_vert.npy"),
        os.path.join(input_dir, "distance_huron.npy"),
        os.path.join(input_dir, "distance_texel.npy"),
        os.path.join(input_dir, "distance_dd.npy")
    ]
    all_correlation(test_files)
    """
    pass
