"""
This module provides basic distance functions.

License
-------
This file is part of "Comparing Texture" http://algomus.fr/code 
Copyright (C) 2021-2023 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Comparing Texture" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Comparing Texture" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Comparing Texture". If not, see
http://www.gnu.org/licenses/
"""
import numpy as np
from typing import List, Any



def manhattan(a, b):
    """
    Compute the manhattan distance between vectors a and b.

    Parameters
    ----------
    a : array_like
    b : array_like

    Note
    ----
    a and b must have the same size.
    """
    # Other version: np.linalg.norm(a-b, ord=1)
    # Other version: scipy.spatial.distance.cityblock

    distance = 0
    for a_i, b_i in zip(a, b):
        distance += abs(a_i - b_i)

    return distance

def manhattan_weighted(a, b, weights):
    """
    Compute the manhattan distance between vectors a and b.

    Parameters
    ----------
    a : array_like
    b : array_like
    weights : array_like

    Note
    ----
    a, b and weights must have the same size.
    """
    distance = 0
    for a_i, b_i, w_i in zip(a, b, weights):
        distance += w_i * abs(a_i - b_i)

    return distance

def euclidean(a, b):
    """
    Compute the euclidean distance between vectors a and b.

    Parameters
    ----------
    a : array_like
    b : array_like

    Note
    ----
    a, b and weights must have the same size.
    """
    # Other version: math.dist
    # Other version: scipy.spatial.distance.euclidean
    # Other version: np.linalg.norm(a - b) , with a and b as numpy arrays (default norm with ord=2)
    # Other version: np.sqrt(np.sum(np.square(np.array(a) - np.array(b))))
    return (
        sum([
            (a_i - b_i) ** 2
            for (a_i, b_i) in zip(a, b)
        ])
    ) ** 0.5

def minkowski(a, b, ord=2):
    """
    Compute the distance between given arrays, based on the norm
    of given order.

    Parameters
    ----------
    a: np.array
    b: np.array
    ord: {positive int, inf}, optional
        Order of the norm. Defaults to 2 (euclidian norm).
    """
    return np.linalg.norm(a - b, ord=ord)

def hamming(a: List[Any], b: List[Any], norm=False):
    """
    Compute Hamming distance between 1-dimensional array-like a and b.

    Parameters
    ----------
    a : array_like
    b : array_like
    norm : bool, optional
        If true, normalize result between 0 (every element is similar)
        and 1 (every element is different). Defaults to False.

    Note
    ----
    a, b must have the same size.
    """
    if len(a) != len(b):
        raise ValueError("Compared objects must be of equal length.")
    distance = 0
    for a_i, b_i in zip(a, b):
        if a_i != b_i:
            distance += 1
    if norm:
        # Normalization between 0 and 1
        distance /= len(a)
    return distance
