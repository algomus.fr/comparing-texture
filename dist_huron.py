"""
This module provides a distance between musical bars based on
David Huron's textural space presented in "Characterizing Musical Texture",
1989.

The original 2D texture space by Huron is build for polyphonic music
with voice separation. In this module, we aim at estimating similar
behaviors for polyphonic music without voice-separation.


License
-------
This file is part of "Comparing Texture" http://algomus.fr/code 
Copyright (C) 2021-2023 by Louis Couturier, 
louis.couturier@u-picardie.fr

"Comparing Texture" is free software: you can redistribute it 
and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of 
the License, or (at your option) any later version.

"Comparing Texture" is distributed in the hope that it will be 
useful, but WITHOUT ANY WARRANTY; without even the implied warranty 
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

You should have received a copy of the GNU General Public License 
along with "Comparing Texture". If not, see
http://www.gnu.org/licenses/
"""
import numpy as np

from dist_base import minkowski
from descriptors import Onsets, Slices



####
# Huron-based features
####
def onset_synchrony(
        onset_dict, slice_dict, length,
        function='lin', verbose=False):
    """
    Return the ratio of onset synchrony in the extract.

    Parameters
    ----------
    onset_dict : dict {<onset_time:float>: list of <midi_pitch:int>}
    slice_dict : dict {<start_time:float>: list of <midi_pitch:int>}
    length : float
        Duration of the extract, in quarterlength.
    function : str in {'lin', 'cos'}
        Projection function for synchronicity values. These functions
        returns 0 (min) for value x=0 or x=1; 1 (max) for x=0.5, and
        interpolate monotonously in between (linearly or smoothed).

    Returns
    -------
    float
        Value of onset synchrony between 0 (fully asynchronous) and 1
        (totally synchronous).
    """
    if verbose:
        print(f"Analyzing slices {slice_dict} with onsets {onset_dict}.")
    if function == "lin":
        v_function = lambda x: abs(2*x - 1)
    elif function == "cos":
        v_function = lambda x: ((np.cos(2*np.pi*x)/2) + 0.5)
    else:
        raise ValueError(
            f"Unknown value '{function}' for argument 'function'. "\
            "Please select value in {'lin', 'cos'}")

    # Retrieve time information
    slice_times = list(slice_dict.keys())
    slice_times.append(str(length))
    slice_times.sort(key=lambda x:float(x))
    if slice_times[-1] != str(length):
        raise ValueError("Length of the extract must be higher than the onset times in the extract.")
    onset_times = list(onset_dict.keys())
    onset_times.sort(key=lambda x:float(x))

    # Loop on slices
    total_synchrony = 0
    for i_slice in range(len(slice_times) - 1):
        # Get number of notes in the slice (new or sustained)
        nb_notes = len(slice_dict[slice_times[i_slice]])
        # Get number of new notes in the slice (only new onsets)
        for onset_time in onset_times:
            if abs(float(slice_times[i_slice]) - float(onset_time)) < 1e-4:
                # Found matching onset
                nb_onsets = len(onset_dict[onset_time])
                break
        else:
            # No onset found : only sustained notes in the slice
            nb_onsets = 0

        if not nb_notes:
            # By default, we consider equal synchrony/asynchrony in silence
            local_synchrony = 0.5
        else:
            local_synchrony = v_function(nb_onsets/nb_notes)
        slice_duration = float(slice_times[i_slice+1]) - float(slice_times[i_slice])
        # Weight by slice duration
        total_synchrony += slice_duration * local_synchrony
        if verbose:
            print(f"Slice of size {slice_duration} : {nb_onsets} / {nb_notes} : {local_synchrony}")

    # Normalize by total length of the extract
    return total_synchrony / length

def semblant_motions(slice_dict, verbose=False):
    """
    Return the ratio of semblant motions in the extract.
    In this version, a semblant motion between two slices
    is identified when the extremal pitches of the slices
    (except sustained or repeated pitches) are going in
    the same direction (up or down).

    Parameters
    ----------
    slice_dict : dict {<start_time:float>: list of <midi_pitch:int>}

    Returns
    -------
    float
        Ratio of semblant between 0 (all opposite motions) and 1
        (only semblant motions).
    """
    # Retrieve time information
    slice_times = list(slice_dict.keys())
    slice_times.sort(key=lambda x:float(x))

    # Loop on successive slices
    nb_motions = 0
    nb_semblant_motions = 0
    for i_slice in range(len(slice_times) - 1):
        # Retrieve pitch lists
        current_slice = slice_dict[slice_times[i_slice]]
        next_slice = slice_dict[slice_times[i_slice + 1]]
        # Remove repeated/sustained notes (no motion involved)
        current_pitches = [
            pitch for pitch in current_slice
            if pitch not in next_slice
        ]
        next_pitches = [
            pitch for pitch in next_slice
            if pitch not in current_slice
        ]
        if not current_pitches or not next_pitches:
            # Skip silence, repetition, sustained notes
            continue
        if min(current_pitches) < min(next_pitches):
            if max(current_pitches) < max(next_pitches):
                # Going up : to higher register
                nb_semblant_motions += 1
                nb_motions += 1
            elif max(current_pitches) > max(next_pitches):
                # Contracting
                nb_motions += 1 # non-semblant motion
        elif min(current_pitches) > min(next_pitches):
            if max(current_pitches) > max(next_pitches):
                # Going down : to lower register
                nb_semblant_motions += 1
                nb_motions += 1
            elif max(current_pitches) < max(next_pitches):
                # Dilating
                nb_motions += 1 # non-semblant motion
    if nb_motions == 0:
        # By default, silence and monophony are "semblant"
        return 1.0

    return nb_semblant_motions / nb_motions

####
# Distance function
####
def huron_vector(
        notelist, 
        length=4.0,
        beat_duration=1.0,
        whole_fractions_convention=False):
    """
    Return a array_like containing (semblant_motion, onset_synchrony)
    for given extract.
    """
    onsets = Onsets(
        notelist,
        length=length,
        beat_duration=beat_duration,
        whole_fractions_convention=whole_fractions_convention
    ).onset_dict
    slices = Slices(
        notelist,
        length=length,
        beat_duration=beat_duration,
        whole_fractions_convention=whole_fractions_convention
    ).slice_dict

    return np.array([
        semblant_motions(slices),
        onset_synchrony(onsets, slices, length=length)
    ])

def huron_dist(
        notelist1, notelist2,
        length1=4.0, length2=4.0,
        beat_duration1=1.0, beat_duration2=1.0,
        ord=2):
    """
    Compute distance between two notelists based on their
    Huron norm.

    Parameters
    ----------
    notelist1 : list of note dictionaries
        Content of the first musical excerpt to compare.
    notelist2 : list of note dictionaries
        Content of the second musical excerpt to compare.
    ord: {positive int, inf}, optional
        Order of the Minkowski distance used.
        Defaults to 2 (euclidian distance).
    """
    huron_vector1 = huron_vector(notelist1, length1, beat_duration1)
    huron_vector2 = huron_vector(notelist2, length2, beat_duration2)
    
    return minkowski(huron_vector1, huron_vector2, ord=ord)
